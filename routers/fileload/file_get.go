package fileload

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

// 上传单个
func upload(r *gin.Engine) {
	// 给表单限制上传大小 (默认 32 MiB)
	r.MaxMultipartMemory = 8 << 20 // 8 MiB
	r.POST("/upload", func(c *gin.Context) {
		file, err := c.FormFile("file")
		if err != nil {
			c.String(500, "上传文件出错")
		}

		// 上传到指定路径
		c.SaveUploadedFile(file, "C:/desktop/"+file.Filename)
		c.String(http.StatusOK, "fileName:", file.Filename)
	})
}

// 上传多个
func uploadMore(r *gin.Engine) {
	// 给表单限制上传大小 (默认 32 MiB)
	r.MaxMultipartMemory = 8 << 20 // 8 MiB
	r.POST("/uploadMore", func(c *gin.Context) {
		// 获取MultipartForm
		form, err := c.MultipartForm()
		if err != nil {
			c.String(http.StatusBadRequest, fmt.Sprintf("get err %s", err.Error()))
		}

		// 获取所有文件
		files := form.File["files"]
		for _, file := range files {
			// 逐个存
			fmt.Println(file.Filename)
		}
		c.String(200, fmt.Sprintf("upload ok %d files", len(files)))
	})
}
