package model

type Data struct {
	DataId   int    `json:"data_id" gorm:"column:data_id;primaryKey"`
	User     string `json:"user" gorm:"column:user"`
	Password string `json:"password" gorm:"column:password"`
}

func (e *Data) TableName() string {
	return "data"
}
