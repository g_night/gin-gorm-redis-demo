package database

import "github.com/go-redis/redis"

var (
	Redis *redis.Client
)

func InitRedis() (err error) {
	Redis = redis.NewClient(&redis.Options{
		Addr:     "127.0.0.1:6379",
		Password: "", // no password set
		DB:       0,  // use default DB
	})
	_, err = Redis.Ping().Result()
	if err != nil {
		return err
	}
	return nil
}
