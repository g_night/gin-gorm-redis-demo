package database

import (
	"fmt"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
	"log"
)

var Db *gorm.DB

func Init() {
	var err error
	dataConfig := "username:password@tcp(localhost:3306)/gostudy"
	Db, err = gorm.Open("mysql", dataConfig)
	if err != nil {
		fmt.Printf("mysql connect error %v", err)
	}
	Db.DB().SetMaxOpenConns(10) // 设置最大连接数
	Db.DB().SetMaxIdleConns(5)  // 设置闲置情况最大连接数
}

func Close() {
	err := Db.Close()
	if err != nil {
		log.Println(err)
	}
}
