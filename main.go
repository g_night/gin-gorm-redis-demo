package main

import (
	"gin-study/database"
	"gin-study/routers"
	"gin-study/routers/fileload"
	"gin-study/routers/login"
)

func main() {
	// 数据库初始化
	database.Init()
	defer database.Close()

	// 注册路由
	r := routers.Init(login.Router, fileload.Router)
	r.Run(":8080")
}
