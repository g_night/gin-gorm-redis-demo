package databsetest

import (
	"fmt"
	"gin-study/database"
	"testing"
)

func TestInitRedis(t *testing.T) {
	// 初始化
	err := database.InitRedis()
	if err != nil {
		fmt.Println(err.Error())
	}

	// 存值(key, value, expire)
	if err := database.Redis.Set("key", "value", 0).Err(); err != nil {
		fmt.Println(err)
	}

	// 取值(key)
	get := database.Redis.Get("key")
	if err := get.Err(); err != nil {
		fmt.Println(err)
	}
	fmt.Println(get.Val())
}
